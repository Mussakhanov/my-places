//
//  StorageManager.swift
//  MyPlaces
//
//  Created by Тимур on 29/08/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import RealmSwift

// для работы с базой сохранение в базу
let realm = try! Realm()

class StorageManager {
    
    static func saveObject(_ place: Place) {//принимает один параметр с типам  Place
        try! realm.write {
            realm.add(place)
        }
    }
    
    //метод для удаления из базы
    
    static func deleteObject(_ place: Place) {//принимает один параметр с типам  Place
        try! realm.write {
            realm.delete(place)
        }
    }
}
