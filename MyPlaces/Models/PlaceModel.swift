//
//  PlaceModel.swift
//  MyPlaces
//
//  Created by Тимур on 30/07/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import RealmSwift//импортируем RealmSwift вместо UIKit


class Place: Object {//структуры не требуют инициализаторов, сборник свойств для хранения
    
    @objc dynamic var name = ""
    @objc dynamic var location: String?
    @objc dynamic var type: String?
    @objc dynamic var imageData: Data?
    @objc dynamic var date = Date()//для сортировки по дате добавления
    @objc dynamic var rating = 0.0
    
    convenience init(name: String, location: String?, type: String?, imageData: Data?, rating: Double) {//назначенный на этот класс инициализатор
        self.init()//сначала инициализируется свойства класса поумолчанию
        self.name = name
        self.location = location
        self.type = type
        self.imageData = imageData
        self.rating = rating
    }
    
    
    
    
    
    
//    let restaurantNames = [
//        "Burger Heroes", "Kitchen", "Bonsai",
//        "X.O", "Sherlock Holmes", "Speak Easy", "Morris Pub",
//    ]
//
//     func savePlaces() {
//
//        for place in restaurantNames {
//
//            //Коонвертация типа UIImage в тип data
//            let image = UIImage(named: place)//берем изображение по имени из цикла
//            guard let imageData = image?.pngData() else { return }
//
//
//            let newPlace = Place()
//
//            newPlace.name = place
//            newPlace.location = "Ufa"
//            newPlace.type = "restaurant"
//            newPlace.imageData = imageData
//
//            StorageManager.saveObject(newPlace)//вызываем метод который сохранит данные в базу
//        }
//
//    }
}
