//
//  TableViewController.swift
//  MyPlaces
//
//  Created by Тимур on 29/07/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import UIKit
import RealmSwift

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private let searchController = UISearchController(searchResultsController: nil)//nil говорит о том что результат поиска будет отображаться на том же viewControllere что и основной контент
    private var places: Results<Place>!//позволяет рабоать с данными тут же записывая и тут же считывать
    private var filteredPlaces: Results<Place>!//коллекция для отображения рельтата поиска
    private var ascendingSorting = true//сортировка по возрасатнию
    
    private var searchBarIsEmpty: Bool {//является ли строка поиска пустой
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    private var isFiltering: Bool {//отслеживания активации поискового запроса
        return searchController.isActive && !searchBarIsEmpty//поисковая строка активирована и не является пустой
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    @IBOutlet var reversedSortingButton: UIBarButtonItem!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        places = realm.objects(Place.self)//тип данных, вытаскиваем из базы обьекты
        
        //Setup the search controller
        searchController.searchResultsUpdater = self//получателем информации об изменении текста в поисковой строке должен быть наш класс
        searchController.obscuresBackgroundDuringPresentation = false//позволяет взаимодействовать с контентом в результатах поиска
        searchController.searchBar.placeholder = "Поиск"//название строки поиска
        navigationItem.searchController = searchController//строка поиска будет интегрированна в NavigationBar
        definesPresentationContext = true//отпустить строку поиска при переходи на другой экран
    }

    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {//если свойство isFiltering имеет значение true то будем возвращать кол-во элементов filteredPlaces
            return filteredPlaces.count
        }
        
        return places.count//если обьект пустой возвращаем 0 иначе кол-во строк по кол-ву элементов массива
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell//обращаемся(приводим) к классу CustomTableViewCell

        
        
        let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]//pаменили на одну строчку "код ниже"
        
//        var place = Place()
        
//        if isFiltering {//если поисковой запрос активирован
//            place = filteredPlaces[indexPath.row]//то будем присваивать обьекту place значение из массива filteredPlaces по индексу текущей строки
//        } else {
//            place = places[indexPath.row]//иначе из коллекции places по индексу строки
//        }
        
        
        
        print(indexPath.row)

        cell.nameLabel.text = place.name//сапостовляем строку к элементу массива по индексу
        cell.locationLabel.text = place.location
        cell.typeLabel.text = place.type
        cell.imageOfPlace.image = UIImage(data: place.imageData!)
        cell.cosmosView.rating = place.rating

//        if place.image == nil {
//            cell.imageOfPlace.image = UIImage(named: place.restaurantImage!)//вставляем картинку из массива по имени
//        } else {
//            cell.imageOfPlace.image = place.image//иначе вставляем выбранную пользователем
//        }



        

        return cell
    }

    
    //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //метод вызова различные пункты свай по ячейки слева на право
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let place = places[indexPath.row]//обращаемся к строке "к ячейки"
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
            StorageManager.deleteObject(place)//удаление с базы
            tableView.deleteRows(at: [indexPath], with: .automatic)//удаление строки
            }
        
        return [deleteAction]//возвращаем массив действия delete
    }

   
    
     //MARK: - Navigation
    
    //переход с одного ViewControllera на другой
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {//если id сигвея будет равен showDetail
            guard let indexPath = tableView.indexPathForSelectedRow else { return }//извлекаем индекс текущей строки(ячейки)
            
            
            let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]//потимизация кода, заменили одной строкой код ниже
//            let place: Place//извлекаем обьект по индексу строки(ячейки)
//            if isFiltering {//если строка поиска активированна
//                place = filteredPlaces[indexPath.row]//то будем присваивать обьекту place значения из коллекции filteredPlaces ао индексу строки
//            } else {
//                place = places[indexPath.row]//иначе будем присваивать этому же обьекту из коллекции places по индексу текущей строки
//            }
            
            let newPlaceVC = segue.destination as! NewPlaceViewController//создем экземпляр класса NewPlaceViewController
            newPlaceVC.currentPlace = place// передаем данные из выбранной ячейки на NewPlaceViewController
        }
    }


    
    @IBAction func unwindSegue(_ segue: UIStoryboardSegue) {// Нажатие на кнопку Save
    
        guard let newPlaceVC = segue.source as? NewPlaceViewController else { return }//через оператор guard извлекаем опционал экземпляр класса NewOlaceViewController
        
        newPlaceVC.savePlace()//вызываем метод saveNewPlace
        //places.append(newPlaceVC.newPlace!)//добавляем в массив places новые данные введенные пользователем
        tableView.reloadData()//обновляем интерфейс
    
}
    
    @IBAction func sortSelection(_ sender: UISegmentedControl) {
        
        sorting()
    }
    
    
    @IBAction func reversedSorting(_ sender: Any) {
        
        ascendingSorting.toggle()//меняет значения на противоположное
        
        //выбор изображения
        if ascendingSorting {
            reversedSortingButton.image = #imageLiteral(resourceName: "AZ")
        } else {
            reversedSortingButton.image = #imageLiteral(resourceName: "ZA")
        }
        
        sorting()
    }
    
    private func sorting() {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            places = places.sorted(byKeyPath: "date", ascending: ascendingSorting)
        } else {
            places = places.sorted(byKeyPath: "name", ascending: ascendingSorting)
        }
        
        tableView.reloadData()//обновления таблицы
    }
}

//расширение ПОИСКА для класса
extension TableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)//метод описанный ниже в который подстовляем параметрзначение из поисковой строки
    }
    
    //фильтрация контента в соответсвии поискового запроса
    private func filterContentForSearchText(_ searchText: String) {
        
        filteredPlaces = places.filter("name CONTAINS[c] %@ OR location CONTAINS[c] %@", searchText, searchText)//поиск по Названию и по Локализации
        
        tableView.reloadData()//обновления TableView
    }
}
