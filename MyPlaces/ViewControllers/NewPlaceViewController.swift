//
//  NewPlaceViewController.swift
//  MyPlaces
//
//  Created by Тимур on 31/07/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import UIKit

class NewPlaceViewController: UITableViewController {
    
    var currentPlace: Place!//свойство в которое передаем обьект с типам Place
    var imageIsChanged = false

    @IBOutlet var saveButton: UIBarButtonItem!
    @IBOutlet var placeName: UITextField!
    @IBOutlet var placeLocation: UITextField!
    @IBOutlet var placeType: UITextField!
    @IBOutlet var placeImage: UIImageView!
    @IBOutlet var ratingControl: RatingControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DispatchQueue.main.async {
//            self.newPlace.savePlaces()//вызываем метод сохранения в базу временных данных
//        }
        

        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))//tableView превратился в обычный вью без разлиновки - без полос
        
        saveButton.isEnabled = false//кнопка save сверху справа будет скрыта поумолчанию
        
        placeName.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)//при редактировании текстового поля placeName будет срабатывать этот метод который в свою очередь будет вызывать метод textFieldChanged
        
        setupEditScreen()//вызываем метод подстовления значений, описанный ниже
        

    }

    //MARK: Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {//метод который отрабатывает клики по ячейкам
        
        if indexPath.row == 0 {//если тапаем по первой ячейке то выскакивает меню если по всем остальным трем то скрывается клавиатура`
            
            let cameraIcon = #imageLiteral(resourceName: "camera")//выбираем ImageLiteral что б вышла картинка
            let photoIcon = #imageLiteral(resourceName: "photo")//выбираем ImageLiteral что б вышла картинка
            
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Camera", style: .default) { _ in
                self.chooseImagePicker(source: .camera)//вызов камеры в сплывашке
            }
            camera.setValue(cameraIcon, forKey: "image")//добавляем списку меню картинку
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")//делаем надпись слева возле картинки а не по центру
            
            
            let photo = UIAlertAction(title: "Photo", style: .default) { _ in
                self.chooseImagePicker(source: .photoLibrary)//вызов галереи в сплывашке
            }
            photo.setValue(photoIcon, forKey: "image")//добавляем списку меню картинку
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")//делаем надпись слева возле картинки а не по центру
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel)//отмена вызова меню
            
            //вписываем все пользовательские действия в этот AlertController
            actionSheet.addAction(camera)
            actionSheet.addAction(photo)
            actionSheet.addAction(cancel)
            
            present(actionSheet, animated: true)//вызов actionSheet
        } else {
            view.endEditing(true)//скрытие клавиатуры
        }
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier, let mapVC = segue.destination as? MapViewController else { return }
        
        mapVC.incomeSegueIdentifier = identifier
        mapVC.mapViewControllerDelegate = self
        
        
        if identifier == "showPlace" {
            mapVC.place.name = placeName.text!
            mapVC.place.location = placeLocation.text
            mapVC.place.type = placeType.text
            mapVC.place.imageData = placeImage.image?.pngData()
        }
        
    }
    
    
    //метод который при нажатии на кнопку Save будет сохранять даные в свойства нашей модели
    func savePlace() {//теперь метод сохраняет не только новые данные но и изменные данные
    
        
        //позволяет вставить картинку поумолчанию  если ничего не выбрали
        let image = imageIsChanged ? placeImage.image : #imageLiteral(resourceName: "imagePlaceholder")//код ниже заменен кодом в одну строчку
//        var image: UIImage?
//        if imageIsChanged {
//            image = placeImage.image
//        } else {
//            image = #imageLiteral(resourceName: "imagePlaceholder")
//        }
        
        let imageData = image?.pngData()// конвертация изображения в тип Data
        
        let newPlace = Place(name: placeName.text!, location: placeLocation.text, type: placeType.text, imageData: imageData, rating: Double(ratingControl.rating))
        
        if currentPlace != nil {
            try! realm.write {
                currentPlace?.name = newPlace.name
                currentPlace?.location = newPlace.location
                currentPlace?.type = newPlace.type
                currentPlace?.imageData = newPlace.imageData
                currentPlace?.rating = newPlace.rating
            }
        } else {
            StorageManager.saveObject(newPlace)//сохраняем в базу обьект
        }
        
        
//        newPlace = Place(name: placeName.text!,
//                         location: placeLocation.text,
//                         type: placeType.text,
//                         image: image,
//                         restaurantImage: nil)
    }
    
    private func setupEditScreen() {
        if currentPlace != nil {
            
            setupNavigationBar()//] вызываем метод описанный ниже (Навигация бара)
            
            imageIsChanged = true//если редактируем запись наше изображение не будет менятся на изображение по умолчанию
            
            guard let data = currentPlace?.imageData, let image = UIImage(data: data) else { return }//тип data приводим к типу UIImage
            
            //подстовляем значения
            placeImage.image = image
            placeImage.contentMode = .scaleAspectFill//редактирования отображения изображения масштабирование
            placeName.text = currentPlace?.name
            placeLocation.text = currentPlace?.location
            placeType.text = currentPlace?.type
            ratingControl.rating = Int(currentPlace.rating)
        }
    }
    
    //Навигация бара
    private func setupNavigationBar() {
        
        if let topItem = navigationController?.navigationBar.topItem {//меняем заголовок кнопки возврата
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        navigationItem.leftBarButtonItem = nil//убераем кнопку cancel
        title = currentPlace?.name//передаем заголовку текущее название заведения
        saveButton.isEnabled = true//кнопка save всегда доступна
    }
    
    //метод позволяет закрыть NewPlaceViewConroller по нажатию на кнопку cancel и возвращается на главный экран
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

//MARK: Text field delegate

extension NewPlaceViewController: UITextFieldDelegate {//для работы с клавиатурой
    
    //скрываем клавиатуру по нажатию на Done
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //этот метод следит заполнино ли текстовое поле placeName что б сделать кнопку Save активной
    @objc private func textFieldChanged() {
        if placeName.text?.isEmpty == false {
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
    }
    
}


//MARK: Work with image

extension NewPlaceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func chooseImagePicker(source: UIImagePickerController.SourceType) {//выбор источника  куда мы кликнем
        if  UIImagePickerController.isSourceTypeAvailable(source) {//проверяем доступность выбора
            let imagePicker = UIImagePickerController()//создаем экземпляр класса
            
            imagePicker.delegate = self
            
            imagePicker.allowsEditing = true//позволяет редактировать изображение
            imagePicker.sourceType = source//определяем тип источника на что нажали что выбрали
            present(imagePicker, animated: true)//для того что бы отобразить любой вью контроллер необходио его вызвать
        }
        
    }
    
    //реализация метода протокола UIImagePickerControllerDelegate
    //выбор картинки по клику я на первую ячеку
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        placeImage.image = info[.editedImage] as? UIImage//получает и использует отредактированное изображение
        placeImage.contentMode = .scaleAspectFill//что позволяет масштабировать изображение по содержимому UIImage
        placeImage.clipsToBounds = true//обрезаем изображение по границе UIImage
        
        imageIsChanged = true
        
        dismiss(animated: true)//закрыть ImagePickerController
    }
}

extension NewPlaceViewController: MapViewControllerDelegate {
    
    func getAddress(_ address: String?) {
        placeLocation.text = address
    }

}
