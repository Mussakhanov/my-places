//
//  CustomTableViewCell.swift
//  MyPlaces
//
//  Created by Тимур on 30/07/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import UIKit
import Cosmos

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imageOfPlace: UIImageView! {
        didSet {
            imageOfPlace.layer.cornerRadius = imageOfPlace.frame.size.height / 2//скруглили углы imageView  а не картинки, высота ячейки
            imageOfPlace.clipsToBounds = true//обрезать изображения по границы
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet var cosmosView: CosmosView! {
        didSet {
            cosmosView.settings.updateOnTouch = false
        }
    }
}
